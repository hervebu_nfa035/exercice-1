package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.util.Base64;
import java.io.FileInputStream;
import java.io.BufferedInputStream;

public class ImageSerializerBase64Impl implements ImageSerializer {

	@Override
	/**
	 * Method serialize an image (File) towards String
	 *  @
	 *  @param image : file containing image to serialize
	 *  @return serialization result as a String
	 */
	public String serialize(File image) throws Exception {
		// TODO Auto-generated method stub
		FileInputStream fis = new FileInputStream(image);
		BufferedInputStream bis = new BufferedInputStream(fis);
		byte[] fileContents = bis.readAllBytes();
		bis.close();
		String encodedString = Base64.getEncoder().encodeToString(fileContents);
		// ObjectInputStream ois = new ObjectInputStream(bis);
		return 	encodedString;	                
		
		// return ois.readUTF();
		
		// FileOutputStream fos = new FileOutputStream(image);
		// ObjectOutputStream oos = new ObjectOutputStream(fos);
		// oos.writeObject(image);
	
		
		
		
		
	}

	@Override
	/**
	 * Method deserialize
	 *  deserialize an encodedImage (String) towards byte array
	 *  @param encodedImage : String to deserialize
	 *  @return decoded image in a byte array 
	 *  
	 */
	public byte[] deserialize(String encodedImage) throws Exception {
		// TODO Auto-generated method stub
		// 
		byte[] image = Base64.getDecoder().decode(encodedImage);
		
		return image;
		   
	    
	}

}
